// SPDX-License-Identifier: UNLICENCED
pragma solidity ^0.6.12;

contract Structs {
	struct CProvider {
        string name;
        bool Allowed;

        uint32 maxRam;
        uint24 maxCpus;
        uint24 maxGpus;
        uint24 maxRunning;

        uint32 usedRam;
        uint24 usedCpus;
        uint24 usedGpus;
        uint24 usedInstances;

        uint32 id;
        address addr;
    }

    struct AiMethod {
        address creator;
        string name;
        uint32 ram;
        uint24 cpu;
        bool gpuRequired;
        uint price;
        bool Allowed;
        bool Active;
        string ipfsCID;
        bool onlyAllowedUsers;
        uint32 id;
        bool deleted;
		string dockerHubLink;
    }

    struct LockedFunds {
        uint ethProvider;
        address provider;
        uint ethAiCreator;
        address aiCreator;
        uint ethFee;
        uint32 releaseBlock;
        address buyer;
        uint32 timeInSec;
        uint32 methodId;
		string complaintText;
    }
	
	struct BuyAiData {
        uint32 aiMethodId;
		address provider;
		uint containerPrice;
		uint aiMethodPrice;
		uint24 timeInSec;
		string videoStreamUrl;
        uint32 dealId;
        uint256 blockNumber;
        uint256 msgValue;
        address msgSender;
    }
}